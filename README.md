One line command to create empty or folder from file

For instructions visit link: https://hemanth22hemu.wordpress.com/2017/02/13/create-a-empty-file-or-folder-with-bash-script/

Note:

Hello everyone,

A note before runnning above script
use path format as /xyx/jade/

don't use format as /xyz/jade

It will create empty like jadefilename 
which you have mention in text file.

#Travis CI Live Build status

Master Branch|[![Travis CI logo](TravisCI.png)](https://travis-ci.org)
---|---
[![DockerCentOS](DockerCentOS.png)](https://travis-ci.org/hemanth22)|[![Build Status](https://travis-ci.org/hemanth22/Creating-Empty-file-or-folder.svg?branch=master)](https://travis-ci.org/hemanth22/Creating-Empty-file-or-folder)

Branch | Code Coverage
---|---
master|[![Codacy Badge](https://api.codacy.com/project/badge/Grade/a7139dea454e4a46902b95ee375b07b8)](https://www.codacy.com/app/hemanth22hemu/Creating-Empty-file-or-folder?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=hemanth22/Creating-Empty-file-or-folder&amp;utm_campaign=Badge_Grade)


